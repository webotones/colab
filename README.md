# Actualización de la Web de Otones
Se trata de actualizar la [Web de Otones de Benjumea](https://otones.es/),
añadiendo y modificando contenidos, según vaya surgiendo información de interés.
La información detallada de los procedimientos está disponible en la
[wiki](https://gitlab.com/webotones/colab/wikis/home).
